package net.minevn.simplewarp;

import net.minevn.simplewarp.warp.WarpManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public class SimpleWarp extends JavaPlugin {
    private static SimpleWarp instance;
    private WarpManager warpManager;

    public static SimpleWarp getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        saveDefaultConfig();
        this.warpManager = new WarpManager(this);
        Objects.requireNonNull(getCommand("warp")).setExecutor(new Command(this));

        getWarpManager().load();
    }

    public WarpManager getWarpManager() {
        return warpManager;
    }
}
