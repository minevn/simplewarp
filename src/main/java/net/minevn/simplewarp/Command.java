package net.minevn.simplewarp;

import org.bukkit.Location;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class Command implements CommandExecutor {
    private final SimpleWarp main;

    public Command(SimpleWarp main) {
        this.main = main;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, org.bukkit.command.@NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (sender instanceof Player player) {
            if (args.length == 0) {
                player.sendMessage(Utils.color("&e/warp setwarp &7- &bTạo warp mới tại vị trí đang đứng"));
                player.sendMessage(Utils.color("&e/warp menu &7- &bMở menu warp"));
                return true;
            }

            if (args[0].equalsIgnoreCase("setwarp")) {
                if (!player.hasPermission("simplewarp.admin")) {
                    player.sendMessage(Utils.color("(&6&lMINEVN &b&l&o> &cBạn không có quyền sử dụng lệnh này!"));
                    return true;
                }

                if (args.length < 2) {
                    player.sendMessage(Utils.color("&cChưa đặt tên warp, sử dụng /warp setwarp <tên>"));
                    return true;
                }

                var warpConfigs = main.getWarpManager().getWarpConfigs();
                String warpID = args[1].toLowerCase();
                String warpName = args[1];
                Location warpLocation = player.getLocation();

                warpConfigs.createSection(warpID);
                warpConfigs.set(warpID + ".name", warpName);
                warpConfigs.set(warpID + ".icon", "PAPER");
                warpConfigs.set(warpID + ".data", 0);
                warpConfigs.set(warpID + ".location", Utils.serializeLocation(warpLocation));
                warpConfigs.set(warpID + ".desc", "");
                main.saveConfig();
                main.reloadConfig();
                main.getWarpManager().load();
                player.sendMessage(Utils.color("&aĐã set warp"));
            } else if (args[0].equalsIgnoreCase("menu")) {
                GUI gui = new GUI(main, player);
            }
        } else {
            sender.sendMessage("Chỉ dùng được trong game!");
            return true;
        }

        return true;
    }
}
