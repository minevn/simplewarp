package net.minevn.simplewarp;

import org.bukkit.*;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("deprecation")
public class Utils {
    private static final SimpleWarp main = SimpleWarp.getInstance();

    @Contract("_ -> new")
    public static @NotNull String color(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }

    public static @NotNull List<String> color(@NotNull List<String> msg) {
        List newList = new ArrayList<>();
        msg.forEach(m -> newList.add(color(m)));

        return newList;
    }

    public static @NotNull ItemStack toItemStack(Material material, String name, List<String> desc, short data) {
        ItemStack is = new ItemStack(material, 1, data);
        ItemMeta im = is.getItemMeta();
        im.setDisplayName(name);
        im.setLore(desc);
        im.setUnbreakable(true);
        im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        im.addItemFlags(ItemFlag.HIDE_UNBREAKABLE);
        is.setItemMeta(im);

        return is;
    }

    public static @Nullable Location deserializeLocation(String location) {
        if (location != null) {
            String[] data = location.split(",");
            World world = Bukkit.getWorld(data[0]);
            double x = Double.parseDouble(data[1]);
            double y = Double.parseDouble(data[2]);
            double z = Double.parseDouble(data[3]);
            float yaw = Float.parseFloat(data[4]);
            float pitch = Float.parseFloat(data[5]);

            return new Location(world, x, y, z, yaw, pitch);

        } else {
            return null;
        }
    }

    public static @NotNull String serializeLocation(@NotNull Location loc) {
        String world = loc.getWorld().getName();
        double x = loc.getX();
        double y = loc.getY();
        double z = loc.getZ();
        float yaw = loc.getYaw();
        float pitch = loc.getPitch();

        return world + "," + x + "," + y + "," + z + "," + yaw + "," + pitch;
    }
}
