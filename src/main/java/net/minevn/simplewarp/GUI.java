package net.minevn.simplewarp;

import net.minevn.guiapi.GuiInventory;
import net.minevn.guiapi.GuiItemStack;
import net.minevn.simplewarp.warp.Warp;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.Map;

public class GUI extends GuiInventory {
    private final SimpleWarp main;
    private final Player viewer;
    private final Map<Integer, Warp> content;

    public GUI(SimpleWarp main, @NotNull Player viewer) {
        super(45, "Chọn map");
        this.main = main;
        this.viewer = viewer;
        this.content = new LinkedHashMap<>();
        build();
        viewer.openInventory(getInventory());
    }

    public void build() {
        getContent().clear();
        int slot = 0;
        for (String id : main.getWarpManager().getWarps().keySet()) {
            Warp warp = main.getWarpManager().getWarps().get(id);
            ItemStack icon = warp.getIcon();
            getContent().put(slot, warp);
            setItem(slot, new GuiItemStack(icon) {
                @Override
                public void onClick(InventoryClickEvent e) {
                    Player p = (Player) e.getWhoClicked();
                    int clickedSlot = e.getSlot();
                    getContent().get(clickedSlot).teleport(p);
                }
            });
            slot++;
        }
    }

    public Player getViewer() {
        return viewer;
    }

    public Map<Integer, Warp> getContent() {
        return content;
    }
}
