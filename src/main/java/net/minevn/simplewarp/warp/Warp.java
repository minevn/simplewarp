package net.minevn.simplewarp.warp;

import net.minevn.simplewarp.Utils;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class Warp {
    private final String id;
    private final String name;
    private final List<String> description;
    private final Location location;
    private final ItemStack icon;

    public Warp(String id, String name, List<String> description, Location location, ItemStack icon) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.location = location;
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<String> getDescription() {
        return description;
    }

    public Location getLocation() {
        return location;
    }

    public ItemStack getIcon() {
        return icon;
    }

    public void teleport(@NotNull Player p) {
        p.teleportAsync(getLocation());
        p.sendMessage(Utils.color("&6&lMINEVN &b&l&o> &aDịch chuyển đến: " + getName()));
    }
}
