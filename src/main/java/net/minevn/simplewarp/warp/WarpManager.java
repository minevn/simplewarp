package net.minevn.simplewarp.warp;

import net.minevn.guiapi.XMaterial;
import net.minevn.simplewarp.SimpleWarp;
import net.minevn.simplewarp.Utils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class WarpManager {
    private final SimpleWarp main;
    private final Map<String, Warp> warps;
    private final FileConfiguration warpConfigs;

    public WarpManager(@NotNull SimpleWarp main) {
        this.main = main;
        this.warps = new LinkedHashMap<>();
        this.warpConfigs = main.getConfig();
    }

    public void load() {
        getWarps().clear();
        getWarpConfigs().getKeys(false).forEach(id -> {
            try {
                var w = warpConfigs.getConfigurationSection(id);
                String name = Utils.color(w.getString("name"));
                Material material = XMaterial.quickMatch(w.getString("icon"));
                short data = (short) w.getInt("data");
                Location location = Utils.deserializeLocation(w.getString("location"));
                List<String> desc = Utils.color(w.getStringList("desc"));

                ItemStack icon = Utils.toItemStack(material, name, desc, data);
                getWarps().put(id, new Warp(id, name, desc, location, icon));
                main.getLogger().info("Loaded warp " + id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public Map<String, Warp> getWarps() {
        return warps;
    }

    public Warp get(String id) {
        return getWarps().get(id);
    }

    public FileConfiguration getWarpConfigs() {
        return warpConfigs;
    }
}
